## References

- [How to Create and Manage Btrfs File System in Linux](https://www.tecmint.com/create-btrfs-filesystem-in-linux/)
- [Convert ISO images to docker images](https://dev.to/sofianehamlaoui/convert-iso-images-to-docker-images-18jh)
- [How To: Unmount an ISO Image in Linux](https://www.cyberciti.biz/faq/unmount-iso-image-linux/)
- [Make customized Alpine Linux rootfs (base image) for containers](https://github.com/alpinelinux/alpine-make-rootfs)
- [Simple script for creating single-layer OCI images.](https://github.com/jirutka/sloci-image/)
- [Use Docker and Alpine Linux to build lightweight containers](https://www.techtarget.com/searchitoperations/tutorial/Use-Docker-and-Alpine-Linux-to-build-lightweight-containers)
- [How to Attach and Mount an EBS volume to EC2 Linux Instance](https://devopscube.com/mount-ebs-volume-ec2-instance/)
- [Attach an Amazon EBS volume to an instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-attaching-volume.html)
- [Set up EC2 Instance Connect](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-connect-set-up.html)
- [Monitor the status of your volumes](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/monitoring-volume-status.html)
- [Create your EC2 resources and launch your EC2 instance](https://docs.aws.amazon.com/efs/latest/ug/gs-step-one-create-ec2-resources.html)
- [Manually installing SSM Agent on Amazon Linux 2 instances](https://docs.aws.amazon.com/systems-manager/latest/userguide/agent-install-al2.html)
- [Amazon EBS Elastic Volumes](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-modify-volume.html)
- [Building a container from scratch in Go - Liz Rice (Microscaling Systems)](https://www.youtube.com/watch?v=Utf-A4rODH8)
- [Containers from scratch: The sequel - Liz Rice (Aqua Security)](https://www.youtube.com/watch?v=_TsSmSu57Zo)
- [Linux Application Container Fundamentals](https://nikhilism.com/post/2016/container-fundamentals/)

## CGroups and Namespaces

- [Cgroups, namespaces and beyond: what are containers made from?](https://www.slideshare.net/Docker/cgroups-namespaces-and-beyond-what-are-containers-made-from)
- [Cgroups, namespaces, and beyond: what are containers made from?](https://www.youtube.com/watch?v=sK5i-N34im8)
- [What Are Namespaces and cgroups, and How Do They Work?](https://www.nginx.com/blog/what-are-namespaces-cgroups-how-do-they-work/)
- [Chapter 1. Introduction to Control Groups (Cgroups)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/ch01)
- [Control Groups](https://docs.kernel.org/admin-guide/cgroup-v1/cgroups.html)

## Container Runtime Comparison

- [A Comprehensive Container Runtime Comparison](https://www.capitalone.com/tech/cloud/container-runtime/)
- [Container Runtimes](https://kubernetes.io/docs/setup/production-environment/container-runtimes/)

## Docker

- [Docker Architecture (v1.3)](https://www.slideshare.net/rajdeep/docker-architecturev2)

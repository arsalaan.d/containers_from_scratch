## Introduction

- Containers and associated tools like **Docker and Kubernetes** have been around for some time now.
- They have helped to change how software is developed and delivered in modern application environments.
- Containers make it possible to quickly deploy and run each piece of software in its own segregated environment, without the need to build individual virtual machines (VMs).

- Most people probably give little thought to how containers work under the covers, but I think it’s important to understand the underlying technologies

– it helps to inform our decision‑making processes. And personally, fully understanding how something works just makes me happy!

## Memory cgroup

memory cgroups divives its role into 3 parts:
- accounting
    tracks of pages used by each group
- limits
    limits memory used, like adding hard or soft limits
- tricky details
    keeping track of memory pages swaps, pages in memory for the currect process or kernel took away




